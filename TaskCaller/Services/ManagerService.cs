﻿using System;
using TaskCaller.Models;
using TaskCaller.Repositories;

namespace TaskCaller.Services
{
    public class ManagerService
    {
        private IManagerRespository _managerRespository;
        public ManagerService(IManagerRespository managerRespository)
        {
            _managerRespository = managerRespository;
        }

        public ResultObject Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return new ResultObject("用户名或密码不能为空");
            }
            var flag = _managerRespository.Login(username, password);
            if (!flag)
            {
                return new ResultObject("用户名或密码错误");
            }
            return new ResultObject(true, "登录成功");
        }
    }
}
