﻿using Loogn.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories
{
    public interface IExecuteLogRepository
    {
        long Add(ExecuteLog m);
        OrmLitePageResult<ExecuteLog> SearchList(long taskId, int taskType, int status, int pageIndex, int pageSize);
    }
}
