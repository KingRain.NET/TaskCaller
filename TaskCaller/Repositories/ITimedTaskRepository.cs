﻿using Loogn.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories
{
    public interface ITimedTaskRepository
    {
        long Add(TimedTask m);
        long Update(TimedTask m);
        OrmLitePageResult<TimedTask> SearchList(string name, bool? enable, int pageIndex, int pageSize);
        TimedTask SingleById(long id);
        List<TimedTask> SelectByIds(List<long> ids);
        int Delete(long id);
        Dictionary<string, List<long>> SelectReadyList();
        int ExecuteOne(TimedTask task);

    }
}
