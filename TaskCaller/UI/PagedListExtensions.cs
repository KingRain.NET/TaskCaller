﻿using Loogn.OrmLite;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskCaller.UI
{
    public static class PagedListExtensions
    {
        public static StaticPagedList<TModel> ToStaticPagedList<TModel>(this OrmLitePageResult<TModel> pr)
        {
            return new StaticPagedList<TModel>(pr.List, pr.PageIndex, pr.PageSize, (int)pr.TotalCount);
        }
    }
}
