﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TaskCaller.UI
{
    public class AuthFilterAttribute : IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Path.StartsWithSegments("/login"))
            {


            }

        }
    }
}
