﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskCaller.Models;
using TaskCaller.Services;

namespace TaskCaller.Controllers
{
    public class HomeController : Controller
    {

        ManagerService managerService;
        public HomeController(ManagerService managerService)
        {
            this.managerService = managerService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("/login"), HttpGet]
        public IActionResult Login()
        {
            HttpContext.Session.Remove("auth");
            return View();
        }
        [Route("/login"), HttpPost]
        public IActionResult Login(string username, string password)
        {
            var ro = managerService.Login(username, password);
            if (ro.Success)
            {
                HttpContext.Session.Set("auth", Encoding.UTF8.GetBytes("1"));
                return Redirect("/TimedTask/List");
            }
            else
            {
                ViewBag.msg = ro.Msg;
                return View();
            }
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}